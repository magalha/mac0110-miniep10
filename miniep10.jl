using Test

function somamatrix(matrix)
	soma = 0
	for i in 1:length(matrix)
		soma += matrix[i]
	end
	return soma
end

function gerarsubmatrizes(matrix)
	vetor_sub_matrices = []
	N = size(matrix)[1]
	for i in 1:N
		for j in 1:(N-i+1)
			for k in 1:(N-i+1)
				submatrix = matrix[j:(j+i-1) , k:(k+i-1)]
				push!(vetor_sub_matrices, submatrix)
			end
		end
	end
	return vetor_sub_matrices
end

function max_sum_submatrix(matrix)
	vetor_sub_matrices = gerarsubmatrizes(matrix)
	vetor_matrices_max_sum = []
	soma_max = 0
	for i in 1:length(vetor_sub_matrices)
		if somamatrix(vetor_sub_matrices[i]) > soma_max
			soma_max = somamatrix(vetor_sub_matrices[i])
			vetor_matrices_max_sum = []
			push!(vetor_matrices_max_sum, vetor_sub_matrices[i])
		elseif somamatrix(vetor_sub_matrices[i]) == soma_max
			push!(vetor_matrices_max_sum, vetor_sub_matrices[i])
		end
	end
	return vetor_matrices_max_sum
end

@test max_sum_submatrix([-1  1  1 ; 1 -1  1 ; 1  1 -2]) == Any[[1 1; -1 1], [1 -1; 1 1], [-1 1 1; 1 -1 1; 1 1 -2]]
@test max_sum_submatrix([1 1 1 ; 1 1 1 ; 1 1 1]) == Any[[1 1 1 ; 1 1 1 ; 1 1 1]]
@test max_sum_submatrix([0 -1 ; -2 -3]) == Any[hcat(0)]

println("Tudo Ok")